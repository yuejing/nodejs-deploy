#! /bin/sh

# 设置启动的jar
SERVICE_NAME="app.js"

PRJ_BIN_DIR=$(dirname $(readlink -f "$0"))
SERVICE_HOME=$(dirname $PRJ_BIN_DIR)

LOGS_DIR=$SERVICE_HOME/logs
# 控制台日志
STDOUT_FILE=$SERVICE_HOME/logs/stdout.out

# 判断该服务名称是否已经启动加载了
PIDS=$(ps --no-heading -C node -f --width 1000 | grep $SERVICE_HOME |awk '{print $2}')
if [ -n "$PIDS" ]; then
    echo "ERROR: The $SERVICE_NAME already started!"
    echo "PID: $PIDS"
    exit 1
fi


# 整合JAVA启动参数
NODEJS_EXEC="node $SERVICE_HOME/$SERVICE_NAME"

#建立日志目录
mkdir -p $LOGS_DIR

# 正式启动区
# =====================================
echo -e "Starting the $SERVICE_NAME ...\c"
nohup $NODEJS_EXEC > $STDOUT_FILE 2>&1 &


# 启动后检测
# =====================================
COUNT=0
while [ $COUNT -lt 1 ]; do
    echo -e ".\c"
    sleep 1
       COUNT=$(ps --no-heading -C node -f --width 1000 | grep "$SERVICE_HOME" | awk '{print $2}' | wc -l)
    if [ $COUNT -gt 0 ]; then
        break
    fi
done
echo "OK!"

PIDS=$(ps --no-heading -C node -f --width 1000 | grep "$SERVICE_HOME" | awk '{print $2}')

echo "STDOUT: $STDOUT_FILE"

echo -e "Start success!"

echo "RUN PID: $PIDS"