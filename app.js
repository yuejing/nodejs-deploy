var http = require('http')
var createHandler = require('gitlab-webhook-handler')

var handler = createHandler({ path: '/deploy', secret: 'myHashSecret' }) 
// 上面的 secret 保持和 GitHub 后台设置的一致
 
function run_cmd(cmd, args, callback) {
  var spawn = require('child_process').spawn;
  var child = spawn(cmd, args);
  var resp = "";
 
  child.stdout.on('data', function(buffer) { resp += buffer.toString(); });
  child.stdout.on('end', function() { callback (resp) });
}
 
http.createServer(function (req, res) {
    try {
        handler(req, res, function (err) {
            res.statusCode = 404
            res.end('no such location')
        })
    } catch(e) {
        console.log('\r\n', e, '\r\n', e.stack);
        try {
          res.end(e.stack);
        } catch(e) { }
    }
}).listen(7777)
 
handler.on('error', function (err) {
  console.error('Error:', err.message)
})
 
handler.on('push', function (event) {
  console.log('Received a push event for %s to %s',
    event.payload.repository.name,
    event.payload.ref);
  run_cmd('sh', ['./deploy.sh'], function(text){ console.log(text) });
})
